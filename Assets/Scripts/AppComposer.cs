﻿namespace PipelineGenerator
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;

    /// <summary>
    /// Responsible for the whole app lifecycle.
    /// The requirements is 
    /// </summary>
    public class AppComposer
    {
        private readonly IReadOnlyList<IPipelineModelGenerator> modelGenerators;

        private readonly IPipelineViewFactory viewFactory;

        public AppComposer(
            IReadOnlyList<IPipelineModelGenerator> modelGenerators,
            IPipelineViewFactory viewFactory)
        {
            this.modelGenerators = modelGenerators ?? throw new ArgumentNullException(nameof(modelGenerators));
            this.viewFactory = viewFactory ?? throw new ArgumentNullException(nameof(viewFactory));
        }

        public void Compose()
        {
            foreach (var generator in this.modelGenerators)
            {
                // I do not care about exception handling since UnityContext by default would catch display exceptions.
                generator.ModelGeneratedEvent += async model =>
                    {
                        try
                        {
                            await this.viewFactory.CreateView(model, default);
                        }
                        catch (OperationCanceledException)
                        {
                            // We ignore if a task was cancelled
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e);
                            throw;
                        }
                    };
            }
        }
    }
}