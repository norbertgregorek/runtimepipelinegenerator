﻿namespace PipelineGenerator
{
    using System.Buffers;

    using UnityEngine;
    using UnityEngine.Assertions;

    public class AppComposerMonoBehaviour : MonoBehaviour
    {
        private AppComposer appComposer;

        [SerializeField]
        private PipelineViewFactoryProvider viewFactoryProvider;

        [SerializeField]
        private MonoBehaviourEmptyViewCreator emptyViewCreator;

        /// <summary>
        /// How many points we have on curves ?
        /// </summary>
        [SerializeField]
        private int pointsOnCurvesCount = 30;

        /// <summary>
        /// How many points we have on every segment ?
        /// (points when we slice a pipe).
        /// </summary>
        [SerializeField]
        private int circuitPoints = 10;

        /// <summary>
        /// That value is used to properly add tiling for pipe mesh.
        /// It says what is the supported texture in width in meters?
        /// </summary>
        [SerializeField]
        private float realTextureWidth = 1;

        private void Awake()
        {
            Assert.IsNotNull(this.viewFactoryProvider);

            var maxArrayLength = 1 << 20;
            var maxArraysPerBucket = 300;
            var arrayPoolProvider = new ArrayPoolProvider(
                new DotNetArrayPool<Vector3>(ArrayPool<Vector3>.Create(maxArrayLength, maxArraysPerBucket)),
                new DotNetArrayPool<Vector2>(ArrayPool<Vector2>.Create(maxArrayLength, maxArraysPerBucket)),
                new DotNetArrayPool<int>(ArrayPool<int>.Create(maxArrayLength, maxArraysPerBucket)),
                new DotNetArrayPool<Matrix4x4>(ArrayPool<Matrix4x4>.Create(maxArrayLength, maxArraysPerBucket)));

            var meshDescriptionFactory = new PipelineMeshDescriptionFactory(
                arrayPoolProvider,
                new PipeTrajectoryCalculator(this.pointsOnCurvesCount),
                this.realTextureWidth,
                this.circuitPoints);

            this.viewFactoryProvider.Init(
                this.emptyViewCreator,
                meshDescriptionFactory);


            var modelGenerators = GameObject.FindObjectsOfType<MonoBehaviourPipelineModelGenerator>();
            this.appComposer = new AppComposer(modelGenerators, this.viewFactoryProvider.CreateInstance());
            this.appComposer.Compose();
        }
    }
}