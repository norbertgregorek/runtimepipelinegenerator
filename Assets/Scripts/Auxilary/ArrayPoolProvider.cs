﻿namespace PipelineGenerator
{
    using System.Buffers;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    using UnityEngine;

    public interface IArrayPool<T>
    {
        T[] Rent(int minSize);

        void Return(T[] arr, bool clearArray = false);
    }

    // That class was created to easily control if there is not memory leak (not returning arrays)
    public class DotNetArrayPool<T> : IArrayPool<T>
    {
        private ArrayPool<T> pool;

        public DotNetArrayPool(ArrayPool<T> pool)
        {
            this.pool = pool;
        }

        public T[] Rent(int minSize)
        {
            var result = this.pool.Rent(minSize);
            return result;
        }

        public void Return(T[] arr, bool clearArray = false)
        {
            this.pool.Return(arr, clearArray);
        }
    }

    public class ArrayPoolProvider
    {
        public ArrayPoolProvider(IArrayPool<Vector3> vec3Pool, IArrayPool<Vector2> vec2Pool, IArrayPool<int> intPool, IArrayPool<Matrix4x4> matrixPool)
        {
            this.Vec3Pool = vec3Pool;
            this.Vec2Pool = vec2Pool;
            this.IntPool = intPool;
            this.MatrixPool = matrixPool;
        }

        public IArrayPool<Vector3> Vec3Pool { get; }

        public IArrayPool<Vector2> Vec2Pool { get; }

        public IArrayPool<int> IntPool { get; }

        public IArrayPool<Matrix4x4> MatrixPool { get; }
    }
}