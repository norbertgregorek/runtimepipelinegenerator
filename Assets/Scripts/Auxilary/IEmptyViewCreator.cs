﻿namespace PipelineGenerator
{
    public interface IEmptyViewCreator
    {
        IPipelineView Create();
    }
}