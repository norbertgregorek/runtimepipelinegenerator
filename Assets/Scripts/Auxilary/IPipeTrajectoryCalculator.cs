﻿namespace PipelineGenerator
{
    using System.Collections.Generic;

    using PipelineGenerator.DataStructs;

    using UnityEngine;
    using UnityEngine.Assertions;

    public interface IPipeTrajectoryCalculator
    {
        /// <summary>
        /// Calculate the trajectory (control points) for a single pipe defined by nodes.
        /// It's the caller duty to support proper <see cref="trajectoryPoints"/> length.
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="trajectoryPoints">Array of points that the trajectory will be created</param>
        /// <param name="segmentBeginnings">Optional parameter to add segment beginnings</param>
        void Calculate(IReadOnlyList<Vector3> nodes, IFixedList<Vector3> trajectory, float bendRadius, IFixedList<Matrix4x4> segmentBeginnings = null);

        /// <summary>
        /// Calculate trajectory control point number for a given nodes
        /// </summary>
        /// <param name="nodes"></param>
        int TrajectoryCount(IReadOnlyList<Vector3> nodes);
    }

    public class PipeTrajectoryCalculator : IPipeTrajectoryCalculator
    {
        private readonly int pointsOnCurvesCount;

        public PipeTrajectoryCalculator(int pointsOnCurvesCount)
        {
            this.pointsOnCurvesCount = pointsOnCurvesCount;
        }

        public void Calculate(IReadOnlyList<Vector3> nodes, IFixedList<Vector3> trajectory, float bendRadius, IFixedList<Matrix4x4> segmentBeginnings = null)
        {
            Assert.IsTrue(nodes.Count >= 2);
            var a = nodes[0];

            for (int i = 1; i < nodes.Count - 1; ++i)
            {
                var b = nodes[i];
                var c = nodes[i + 1];

                a = this.ProcesConsecutiveNodes(a, b, c, bendRadius, trajectory);
            }

            // Now we have two remaining points, just add them to trajectory
            trajectory.Add(a);
            trajectory.Add(nodes[nodes.Count - 1]);
        }

        public int TrajectoryCount(IReadOnlyList<Vector3> nodes)
        {
            int trajectoryCount = 0;

            for (int i = 1; i < nodes.Count - 1; ++i)
            {
                var a = nodes[i - 1];
                var b = nodes[i];
                var c = nodes[i + 1];

                trajectoryCount++;
                var dot = Vector3.Dot((b - a).normalized, (c - b).normalized);
                if (!dot.Equals(1f))
                {
                    trajectoryCount += this.pointsOnCurvesCount;
                }
            }

            return trajectoryCount + 2;
        }

        /// <summary>
        /// /// Given three modes consecutive nodes add one straight pipe and one curved,
        /// in case of vectors ba and bc are not parallel
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns>Point from when we will add another segment</returns>
        private Vector3 ProcesConsecutiveNodes(
            Vector3 a, 
            Vector3 b, 
            Vector3 c, 
            float bendRadius,
            IFixedList<Vector3> trajectory)
        {
            trajectory.Add(a);

            var ba = (a - b).normalized;
            var bc = (c - b).normalized;
            if (Vector3.Dot((b - a).normalized, (c - b).normalized).Equals(1.0f))
            {
                return b;
            }

            // Half is a vector that is located on the plane that vectors ba and bc creates 
            // and Angle(half, ba) == Angle(half, bc)
            var half = (ba + bc).normalized;
            var angle = Vector3.Angle(ba, half);

            // In world space
            var sphereCenter = b + half * (bendRadius / Mathf.Sin(angle * Mathf.Deg2Rad));

            // We are looking for the third point in the triangle
            // with vertices 'sphereCenter', 'b', 'x' (unknown).
            // That point is intersection point of bend sphere and ba vector.

            var tmp = (sphereCenter - b).magnitude;
            var xbLength = Mathf.Sqrt(bendRadius * bendRadius + tmp * tmp);

            var x = b + ba * xbLength;
            var y = b + bc * xbLength;

            // x and y are point where we add curved curved pipeline
            // using 3-point bezier curve (with x, b, y points).
            for (float t = 0; t < 1f; t += 1 / (this.pointsOnCurvesCount - 1f))
            {
                trajectory.Add(this.BezierCurve(x, b, y, t));
            }

            return this.BezierCurve(x, b, y, 1f);
        }

        private Vector3 BezierCurve(Vector3 a, Vector3 b, Vector3 c, float t)
        {
            // Instead of hard math formula use bezier interpretation with simple lerp :)
            var v1 = Vector3.Lerp(a, b, t);
            var v2 = Vector3.Lerp(b, c, t);
            return Vector3.Lerp(v1, v2, t);
        }
    }
}