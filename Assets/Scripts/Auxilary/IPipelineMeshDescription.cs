﻿namespace PipelineGenerator
{
    using System;
    using System.Collections.Generic;

    using PipelineGenerator.DataStructs;

    using TMPro;

    using UnityEngine;

    public interface IPipelineMeshDescription : IDisposable
    {
        IFixedList<Vector3> Vertices { get; }

        IFixedList<Vector3> Normals { get; }

        IFixedList<Vector2> UVs { get; }

        IFixedList<int> Triangles { get; }
    }

    public class MultiplePipelineMeshDescription : IDisposable
    {
        public MultiplePipelineMeshDescription(IReadOnlyList<IPipelineMeshDescription> descriptions)
        {
            this.Descriptions = descriptions;
        }

        public IReadOnlyList<IPipelineMeshDescription> Descriptions { get; }

        public void Dispose()
        {
            for (int i = 0; i < this.Descriptions.Count; ++i)
            {
                this.Descriptions[i].Dispose();
            }
        }
    }

    public class ArrayPoolPipelineMeshDescription : IPipelineMeshDescription
    {
        private readonly ArrayPoolProvider arrayPool;

        public ArrayPoolPipelineMeshDescription(
            ArrayPoolProvider arrayPool,
            IFixedList<Vector3> vertices,
            IFixedList<Vector3> normals,
            IFixedList<Vector2> uvs,
            IFixedList<int> triangles)
        {
            this.arrayPool = arrayPool;
            this.Vertices = vertices;
            this.Normals = normals;
            this.UVs = uvs;
            this.Triangles = triangles;
        }

        public IFixedList<Vector3> Vertices { get; }

        public IFixedList<Vector3> Normals { get; }

        public IFixedList<Vector2> UVs { get; }

        public IFixedList<int> Triangles { get; }

        public void Dispose()
        {
            this.arrayPool.Vec3Pool.Return(this.Vertices.Array, true);
            this.arrayPool.Vec3Pool.Return(this.Normals.Array, true);
            this.arrayPool.Vec2Pool.Return(this.UVs.Array, true);
            this.arrayPool.IntPool.Return(this.Triangles.Array, true);
        }
    }
}