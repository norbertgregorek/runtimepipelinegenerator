﻿namespace PipelineGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using PipelineGenerator.DataStructs;

    using UnityEngine;
    using UnityEngine.Assertions;

    /// <summary>
    /// Thread-safe factory
    /// </summary>
    public interface IPipelineMeshDescriptionFactory
    {
        /// <summary>
        /// Creates multiple instances of mesh instances. In fact the number of instances is
        /// the same as <see cref="IPipelineModel.PipesCount"/>.
        /// The better architecture would be to create a decorator/composite pattern over <see cref="IPipelineMeshDescription"/>
        /// but it would be harder to maintain the memory since it was written firstly for assumption of having only one pipe.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<MultiplePipelineMeshDescription> Create(IPipelineModel model, CancellationToken token);
    }

    // Thread-safe since ArrayPool class is a thread safe

    /// <summary>
    /// Creates mesh description with baked information about texture tiling.
    /// </summary>
    public class PipelineMeshDescriptionFactory : IPipelineMeshDescriptionFactory
    {
        private readonly ArrayPoolProvider poolProvider;

        private readonly IPipeTrajectoryCalculator trajectoryCalculator;

        /// <summary>
        /// Number of points on a pipe circuit.
        /// </summary>
        private readonly int circuitPoints;

        /// <summary>
        /// Real texture width in meters. There is an assumption that texture height exactly cover 
        /// </summary>
        private readonly float realTextureWidth;

        public PipelineMeshDescriptionFactory(
            ArrayPoolProvider poolProvider,
            IPipeTrajectoryCalculator trajectoryCalculator,
            float realTextureWidth,
            int circuitPoints)
        {
            this.poolProvider = poolProvider;
            this.trajectoryCalculator = trajectoryCalculator;
            this.realTextureWidth = realTextureWidth;
            this.circuitPoints = circuitPoints;
        }

        public async Task<MultiplePipelineMeshDescription> Create(IPipelineModel model, CancellationToken token)
        {
            IPipelineMeshDescription[] models = new IPipelineMeshDescription[model.PipesCount];

            // If you move Paralle.For outside task when the code will run synchronously and you may experience more realtime solution
            // since every request to update the model will be finished with success

            await Task.Run(() => { Parallel.For(0, 1, i => { }); });
            Parallel.For(
                0,
                model.PipesCount,
                new ParallelOptions { CancellationToken = token },
                i =>
                    {
                        Vector3[] outputPositions = null;
                        try
                        {
                            outputPositions = this.poolProvider.Vec3Pool.Rent(model.NodePositions.Count);
                            var outputPositionList = new FixedList<Vector3>(outputPositions);
                            this.FindNodePositionsForSpecificPipe(
                                model.NodePositions,
                                outputPositionList,
                                i,
                                model.Diameter,
                                model.PipesCount);
                            models[i] = this.CreateSinglePipe(outputPositionList, model.Diameter, token);
                        }
                        finally
                        {
                            this.poolProvider.Vec3Pool.Return(outputPositions);
                        }
                    });

            return new MultiplePipelineMeshDescription(models);
        }

        public IPipelineMeshDescription CreateSinglePipe(IFixedList<Vector3> positions, float diameter, CancellationToken token)
        {
            int pointsCount = this.trajectoryCalculator.TrajectoryCount(positions);
            var trajectory = new FixedList<Vector3>(this.poolProvider.Vec3Pool.Rent(pointsCount));

            this.trajectoryCalculator.Calculate(positions, trajectory, diameter * 4f);

            Assert.IsTrue(pointsCount >= 2);

            // Rent buffers
            var numberOfPoints = (trajectory.Count - 1) * this.circuitPoints * 2;

            var verticesArray = this.poolProvider.Vec3Pool.Rent(numberOfPoints);
            var normalsArray = this.poolProvider.Vec3Pool.Rent(numberOfPoints);
            var uvsArray = this.poolProvider.Vec2Pool.Rent(numberOfPoints);

            // Multiply by 3 since every triangle has three vertices
            var triangleArray = this.poolProvider.IntPool.Rent(numberOfPoints * 3);

            try
            {
                var verticesList = new FixedList<Vector3>(verticesArray);
                var normalsList = new FixedList<Vector3>(normalsArray);
                var uvsList = new FixedList<Vector2>(uvsArray);
                var triangleList = new FixedList<int>(triangleArray);


                float totalPipeLength = 0;

                for (int i = 1; i < trajectory.Count; ++i)
                {
                    if (token.IsCancellationRequested)
                    {
                        // In case of cancellation 
                        token.ThrowIfCancellationRequested();
                    }

                    var prev = trajectory[i - 1];
                    var curr = trajectory[i];
                    var dir = curr - prev;

                    // For every segment add points from the beginning (prev) and end (curr).
                    // Note that we duplicate some vertices some way but it's inevitably since there is only one index on GPU
                    // That matches vertex, uv and normal at the same time.

                    // Duplicate vertices from prev position
                    var vertexCount = verticesList.Count;
                    var prevDir = i > 1 ? trajectory[i - 1] - trajectory[i - 2] : dir;
                    var radius = diameter / 2;
                    this.AddVerticesNormalsAndUvs(
                        prev,
                        prevDir,
                        verticesList,
                        normalsList,
                        uvsList,
                        radius,
                        totalPipeLength);

                    totalPipeLength += (curr - prev).magnitude;
                    this.AddVerticesNormalsAndUvs(
                        curr,
                        dir,
                        verticesList,
                        normalsList,
                        uvsList,
                        radius,
                        totalPipeLength);
                    this.AddFaces(triangleList, vertexCount);
                }

                // Post conditions
                Assert.IsTrue(verticesList.Count == numberOfPoints);
                Assert.IsTrue(uvsList.Count == numberOfPoints);
                Assert.IsTrue(triangleList.Count == 3 * numberOfPoints);

                return new ArrayPoolPipelineMeshDescription(
                    this.poolProvider,
                    verticesList,
                    normalsList,
                    uvsList,
                    triangleList);
            }
            catch
            {
                // When exception return rented buffers
                this.poolProvider.Vec3Pool.Return(verticesArray);
                this.poolProvider.Vec3Pool.Return(normalsArray);
                this.poolProvider.Vec2Pool.Return(uvsArray);
                this.poolProvider.IntPool.Return(triangleArray);
                throw;
            }
            finally
            {
                this.poolProvider.Vec3Pool.Return(trajectory.Array);
            }
        }

        private void AddFaces(IFixedList<int> triangleList, int vertexCount)
        {
            // Triangulation - every segment contains 2 * circuitPoints.
            // every quad is represented:
            /* p2 ****************** p4
               *                     *
               *                     *
               *                     *
               p1 ****************** p3
            where: p1 and p2 are take from the "prev circuit", p3 and pr taken from "curr circuit"
            */

            for (int f = 0; f < this.circuitPoints; f++)
            {
                var p1 = f;
                var p2 = (f + 1) % this.circuitPoints;
                var p3 = p1 + this.circuitPoints;
                var p4 = p2 + this.circuitPoints;

                p1 += vertexCount;
                p2 += vertexCount;
                p3 += vertexCount;
                p4 += vertexCount;

                triangleList.Add(p1);
                triangleList.Add(p2);
                triangleList.Add(p4);

                triangleList.Add(p1);
                triangleList.Add(p4);
                triangleList.Add(p3);
            }
        }

        private void AddVerticesNormalsAndUvs(
            Vector3 center,
            Vector3 forward,
            IFixedList<Vector3> vertices,
            IFixedList<Vector3> normals,
            IFixedList<Vector2> uvs,
            float radius,
            float totalPipeLength)
        {
            var space = this.CalcuateLocalSpace(forward.normalized);

            var deltaAngle = 360f / this.circuitPoints;

            Matrix4x4 rotation = Matrix4x4.identity;
            rotation.SetColumn(0, space.right);
            rotation.SetColumn(1, space.up);
            rotation.SetColumn(2, space.forward);

            for (int i = 0; i < this.circuitPoints; ++i)
            {
                var angle = i * deltaAngle;
                var angleRad = angle * Mathf.Deg2Rad;

                // These points are in local space
                var x = radius * Mathf.Cos(angleRad);
                var y = radius * Mathf.Sin(angleRad);
                var z = 0;

                // Transform to world space
                var point = rotation.MultiplyPoint(new Vector3(x, y, z)) + center;
                vertices.Add(point);

                // Add normals
                normals.Add(rotation.MultiplyPoint(new Vector3(x, y, z)).normalized);

                // Assumption about baked in tiling.
                uvs.Add(new Vector2(totalPipeLength / this.realTextureWidth, angle / 360));
            }
        }

        // Thread-safe since it's a context-free
        private void FindNodePositionsForSpecificPipe(
            IReadOnlyList<Vector3> inputPositions,
            IFixedList<Vector3> outputPositionList,
            int pipeIndex,
            float diameter,
            int pipesCount)
        {
            float spaceBetweenPipes = 1.2f * diameter;
            
            // Displacement on local pipe's coordinate system on X axis.
            // This is a case for even number of pipes.
            // pipeIndex == 0 refers to the right most pipe
            float rightMostPipeDisplacement =
                spaceBetweenPipes / 2 +
                diameter * pipesCount / 2 +
                spaceBetweenPipes * (pipesCount / 2 - 1) -
                diameter / 2;
            
            if (pipesCount % 2 == 1)
            {
                rightMostPipeDisplacement =
                    diameter * (pipesCount / 2) +
                    spaceBetweenPipes * (pipesCount / 2);
            }
            
            var myDisplacament = rightMostPipeDisplacement - (pipeIndex * (diameter + spaceBetweenPipes));
            
            int length = inputPositions.Count;
            for (int i = 0; i < length; ++i)
            {
                // Always assume that there is a prev and next element. If our element is first or last generate additional point.
                Vector3 prev = i == 0 ? inputPositions[0] + (inputPositions[1] - inputPositions[0]) * -1 : inputPositions[i - 1];
                Vector3 curr = inputPositions[i];
                Vector3 next = (i == length - 1) ? inputPositions[i] + (inputPositions[i] - inputPositions[i - 1]) : inputPositions[i + 1];


                //Project prev and curr to the plane with origin in point 'curr' and normal vector (0, 1, 0)
                prev.y = next.y = curr.y;
                var halfVector = ((prev - curr).normalized + (next - curr).normalized).normalized;
                if (halfVector == Vector3.zero)
                {
                    // This is the case for parallel vectors
                    halfVector = Vector3.Cross(Vector3.up, (next - curr).normalized);
                }

                var right = CalcuateLocalSpace((next - curr).normalized).right;
                if (Vector3.Dot(halfVector, right) < 0)
                {
                    halfVector = -halfVector;
                }

                outputPositionList.Add(curr + (halfVector * myDisplacament));
            }

            Assert.IsTrue(outputPositionList.Count == inputPositions.Count);
        }

        private (Vector3 right, Vector3 up, Vector3 forward) CalcuateLocalSpace(Vector3 forward)
        {
            var p = new Plane(Vector3.zero, Vector3.up, forward);

            var right = p.normal;
            var up = Vector3.Cross(forward, right);

            return (right, up, forward);
        }
    }
}