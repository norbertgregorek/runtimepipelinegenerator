﻿namespace PipelineGenerator
{
    public interface IPipelineView
    {
        void UpdateView(MultiplePipelineMeshDescription pipelineMeshDescription);

        void Destroy();
    }
}