﻿namespace PipelineGenerator
{
    using UnityEngine;

    public class MonoBehaviourEmptyViewCreator : MonoBehaviour, IEmptyViewCreator
    {
        [SerializeField]
        private MonoBehaviourPipelineView viewPrefab;

        public IPipelineView Create()
        {
            return GameObject.Instantiate<MonoBehaviourPipelineView>(this.viewPrefab);
        }
    }
}