﻿namespace PipelineGenerator
{
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.Assertions;

    public class MonoBehaviourPipelineView : MonoBehaviour, IPipelineView
    {
        private List<MeshFilter> filters = new List<MeshFilter>(4);

        [SerializeField]
        private Material pipeMaterial;

        public void UpdateView(MultiplePipelineMeshDescription pipelineMeshDescription)
        {
            this.ClearMeshes();

            for (int i = 0; i < pipelineMeshDescription.Descriptions.Count; ++i)
            {
                var description = pipelineMeshDescription.Descriptions[i];
                var meshFilter = this.GetMeshFilter(i);
            
                var mesh = meshFilter.sharedMesh ?? new Mesh();
                Assert.IsNotNull(mesh);
            
                // TODO: Think about a better way to pass mesh data without creating temporary buffers
                mesh.vertices = description.Vertices.Array;
                mesh.uv = description.UVs.Array;
                mesh.normals = description.Normals.Array;
                mesh.SetTriangles(description.Triangles.Array, 0, description.Triangles.Count, 0);
            
                meshFilter.sharedMesh = mesh;
            }
        }

        public void Destroy()
        {
            for (int i = 0; i < this.filters.Count; ++i)
            {
                var filter = this.filters[i];
                GameObject.Destroy(filter.sharedMesh);
                GameObject.Destroy(filter.gameObject);
            }

            this.filters = null;

            GameObject.Destroy(this.gameObject);
        }

        private MeshFilter GetMeshFilter(int indx)
        {
            while (this.filters.Count <= indx)
            {
                // Add new filter
                var go = new GameObject($"Pipe {indx}");
                go.transform.SetParent(this.transform, false);
                var renderer = go.AddComponent<MeshRenderer>();
                renderer.sharedMaterial = this.pipeMaterial;
                
                this.filters.Add(go.AddComponent<MeshFilter>());
            }

            return this.filters[indx];
        }

        private void ClearMeshes()
        {
            for (int i = 0; i < this.filters.Count; ++i)
            {
                this.filters[i].sharedMesh.Clear(false);
            }
        }
    }
}