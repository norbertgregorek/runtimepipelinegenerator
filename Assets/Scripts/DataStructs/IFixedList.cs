﻿namespace PipelineGenerator.DataStructs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public interface IFixedList<T> : IReadOnlyList<T>
    {
        T[] Array { get; }

        void Add(T el);
    }

    public class FixedList<T> : IFixedList<T>
    {
        // These are microoptimisations to reduce cost of getter/setter
        private T[] array;

        private int count;

        public FixedList(T[] array)
        {
            this.array = array ?? throw new ArgumentNullException(nameof(array));
        }

        public T[] Array => this.array;

        public int Count => this.count;

        public T this[int index] => this.array[index];

        public void Add(T el)
        {
            this.array[this.count++] = el;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < this.Count; ++i)
            {
                yield return this[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}