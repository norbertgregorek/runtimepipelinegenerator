﻿namespace PipelineGenerator
{
    using System;

    public interface IPipelineModelGenerator
    {
        event Action<IPipelineModel> ModelGeneratedEvent;
    }
}