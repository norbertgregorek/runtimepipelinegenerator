﻿namespace PipelineGenerator
{
    using System.Collections.Generic;

    using UnityEngine;

    public interface IPipelineModel
    {
        float Diameter { get; }

        ulong Id { get; }

        IReadOnlyList<Vector3> NodePositions { get; }

        int PipesCount { get; }
    }
}