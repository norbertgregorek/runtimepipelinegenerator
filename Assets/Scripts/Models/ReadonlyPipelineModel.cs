﻿namespace PipelineGenerator
{
    using System.Collections.Generic;

    using UnityEngine;

    public class ReadonlyPipelineModel : IPipelineModel
    {
        public ReadonlyPipelineModel(float diameter, ulong id, IReadOnlyList<Vector3> nodePositions, int pipesCount)
        {
            this.Diameter = diameter;
            this.Id = id;
            this.NodePositions = nodePositions;
            this.PipesCount = pipesCount;
        }

        public float Diameter { get; }

        public ulong Id { get; }

        public IReadOnlyList<Vector3> NodePositions { get; set; }

        public int PipesCount { get; set; }
    }
}