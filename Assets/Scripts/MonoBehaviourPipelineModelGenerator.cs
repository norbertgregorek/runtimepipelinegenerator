﻿namespace PipelineGenerator
{
    using System;
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.Assertions;

    using Random = System.Random;

    /// <summary>
    /// Represents a class modeling a <see cref="IPipelineModelGenerator"/>.
    /// Child objects represents node positions. The order of them represents order of nodes.
    /// You could create or remove nodes at runtime and manipulate their positions as well.
    /// </summary>
    public class MonoBehaviourPipelineModelGenerator : MonoBehaviour, IPipelineModelGenerator
    {
        private readonly ulong id;

        [SerializeField]
        private float diameter;

        [SerializeField]
        private int pipesCount;

        private List<Vector3> initialPositions = new List<Vector3>();

        private float? lastDiameter;

        private int? lastPipesCount;

        public MonoBehaviourPipelineModelGenerator()
        {
            this.id = IdGenerator.Generate();
        }

        public event Action<IPipelineModel> ModelGeneratedEvent;


        private void Update()
        {
            bool shouldRefresh = this.lastDiameter != this.diameter || this.lastPipesCount != this.pipesCount;

            // Check children positions
            if (this.transform.childCount != this.initialPositions.Count)
            {
                this.initialPositions.Clear();
                shouldRefresh = true;
                for (int i = 0; i < this.transform.childCount; ++i)
                {
                    this.initialPositions.Add(this.transform.GetChild(i).position);
                }
            }
            else
            {
                for (int i = 0; i < this.transform.childCount; ++i)
                {
                    var childPos = this.transform.GetChild(i).position;
                    if (this.initialPositions[i] != childPos)
                    {
                        shouldRefresh = true;
                        this.initialPositions[i] = childPos;
                    }
                }
            }
            

            if (shouldRefresh)
            {
                this.lastDiameter = this.diameter;
                this.lastPipesCount = this.pipesCount;

                this.ModelGeneratedEvent?.Invoke(
                    new ReadonlyPipelineModel(
                        this.diameter,
                        this.id,
                        this.initialPositions,
                        this.pipesCount));
            }
        }

        /// <summary>
        /// Helper class responsible for generating ids
        /// </summary>
        private static class IdGenerator
        {
            private static Random rand = new Random();

            private static byte[] buffer = new byte[sizeof(ulong)];

            // Source:
            // https://social.msdn.microsoft.com/Forums/vstudio/en-US/cb9c7f4d-5f1e-4900-87d8-013205f27587/64-bit-strong-random-function?forum=csharpgeneral
            public static ulong Generate()
            {
                rand.NextBytes(buffer);
                return BitConverter.ToUInt64(buffer, 0) % ulong.MaxValue;
            }
        }
    }
}