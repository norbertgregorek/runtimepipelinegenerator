﻿namespace PipelineGenerator
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IPipelineViewFactory
    {
        Task CreateView(IPipelineModel model, CancellationToken cancellationToken);

        Task<bool> DestroyView(IPipelineModel model);
    }
}