﻿namespace PipelineGenerator
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using Debug = UnityEngine.Debug;

    /// <summary>
    /// You can run it only from the main thread.
    /// There is an assumption if you run multiple times <see cref="CreateView"/> the previous request is cancelled.
    /// </summary>
    public class PipelineViewFactory : IPipelineViewFactory
    {
        private readonly IEmptyViewCreator emptyViewCreator;

        private readonly IPipelineMeshDescriptionFactory meshDescriptionFactory;

        private Dictionary<ulong, IPipelineView> viewsDict = new Dictionary<ulong, IPipelineView>();

        private Dictionary<ulong, CancellationTokenSource> inProcessRequests = new Dictionary<ulong, CancellationTokenSource>();

        public PipelineViewFactory(IEmptyViewCreator emptyViewCreator, IPipelineMeshDescriptionFactory meshDescriptionFactory)
        {
            this.emptyViewCreator = emptyViewCreator;
            this.meshDescriptionFactory = meshDescriptionFactory;
        }

        public async Task CreateView(IPipelineModel model, CancellationToken cancellationToken)
        {
            this.TryCancelRequest(model.Id);

            // Register new request composing a CancellationToken
            var tokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            this.inProcessRequests[model.Id] = tokenSource;

            using (var description = await this.meshDescriptionFactory.Create(model, tokenSource.Token))
            {
                tokenSource.Token.ThrowIfCancellationRequested();

                if (!this.viewsDict.ContainsKey(model.Id))
                {
                    this.viewsDict.Add(model.Id, this.emptyViewCreator.Create());
                }

                this.viewsDict[model.Id].UpdateView(description);
                this.inProcessRequests.Remove(model.Id);
            }
        }

        public Task<bool> DestroyView(IPipelineModel model)
        {
            if (this.viewsDict.TryGetValue(model.Id, out var view))
            {
                this.viewsDict.Remove(model.Id);
                this.TryCancelRequest(model.Id);
                view.Destroy();
                return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }

        private void TryCancelRequest(ulong id)
        {
            if (this.inProcessRequests.TryGetValue(id, out var request))
            {
                request.Cancel();
                this.inProcessRequests.Remove(id);
            }
        }
    }
}