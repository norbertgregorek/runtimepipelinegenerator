﻿namespace PipelineGenerator
{
    using UnityEngine;
    using UnityEngine.Assertions;

    public class PipelineViewFactoryProvider : MonoBehaviour
    {
        private IEmptyViewCreator emptyViewCreator;

        private IPipelineMeshDescriptionFactory meshDescriptionFactory;

        public void Init(IEmptyViewCreator emptyViewCreator, IPipelineMeshDescriptionFactory meshDescriptionFactory)
        {
            this.emptyViewCreator = emptyViewCreator;
            this.meshDescriptionFactory = meshDescriptionFactory;
        }

        public IPipelineViewFactory CreateInstance()
        {
            Assert.IsNotNull(this.emptyViewCreator);
            Assert.IsNotNull(this.meshDescriptionFactory);

            return new PipelineViewFactory(this.emptyViewCreator, this.meshDescriptionFactory);
        }
    }
}