Sample project that creates 3d pipeline mesh at runtime with little RAM allcation.
Application uses multi-thread and asynchronous environment to speed up calculations (Task and Parallel classes).

1. Open SampleScene in Run mode.
2. Manipulate with control points (spheres on the scene) to update geometry in realtime.